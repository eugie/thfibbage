const express = require("express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);
const path = require("path");

// Game state and data
let currentQuestion = null;
const players = [];
const questions = [
  {
    question: "ชื่อเมืองหลวงของประเทศไทยคือ...",
    correctAnswer: "กรุงเทพมหานคร",
    fakeAnswers: ["เชียงใหม่", "ขอนแก่น", "พัทยา"],
  },
  // Add more questions and answers here
];

// Set up view directory
app.set("views", path.join(__dirname, "views"));

// Serve the index.html file
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "views", "index.html"));
});

// Serve static files (CSS, JS, etc.)
app.use(express.static(path.join(__dirname, "public")));

// Socket.IO connection
io.on("connection", (socket) => {
  console.log("A user connected");

  // Handle new player joining
  socket.on("join", (playerName) => {
    const player = { id: socket.id, name: playerName };
    players.push(player);
    console.log(`${playerName} joined the game`);

    // Send the current question to the new player
    if (currentQuestion) {
      socket.emit("question", currentQuestion);
    }
  });

  // Handle answer submission
  socket.on("answer", (answer) => {
    const player = players.find((p) => p.id === socket.id);
    if (player) {
      console.log(`${player.name} submitted answer: ${answer}`);

      // Check if the submitted answer is correct
      if (
        answer.toLowerCase() === currentQuestion.correctAnswer.toLowerCase()
      ) {
        console.log(`${player.name} answered correctly!`);
        // TODO: Implement score update logic
      } else {
        console.log(`${player.name} answered incorrectly.`);
      }
    }
  });

  // Clean up when a player disconnects
  socket.on("disconnect", () => {
    const player = players.find((p) => p.id === socket.id);
    if (player) {
      console.log(`${player.name} left the game`);
      players.splice(players.indexOf(player), 1);
    }
  });
});

// Start the game loop
const startGame = () => {
  currentQuestion = questions[0]; // Start with the first question
  io.emit("question", currentQuestion);

  // TODO: Implement game loop and question rotation logic
};

// Start the server
const PORT = process.env.PORT || 3000;
http.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
  startGame();
});
