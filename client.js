const socket = io();

// Join the game
const playerName = prompt("Enter your name:");
socket.emit("join", playerName);

// Receive the current question
socket.on("question", (question) => {
  displayQuestion(question);
});

// Submit an answer
const answerInput = document.getElementById("answerInput");
const submitButton = document.getElementById("submitAnswer");

submitButton.addEventListener("click", () => {
  const answer = answerInput.value.trim();
  if (answer) {
    socket.emit("answer", answer);
    answerInput.value = "";
  }
});

// Display the question and answer choices
function displayQuestion(question) {
  const questionElement = document.getElementById("question");
  const answersElement = document.getElementById("answers");

  questionElement.innerText = question.question;
  answersElement.innerHTML = "";

  // Display the correct answer and fake answers as buttons
  const answers = [question.correctAnswer, ...question.fakeAnswers];
  answers.forEach((answer) => {
    const button = document.createElement("button");
    button.innerText = answer;
    answersElement.appendChild(button);
  });
}
